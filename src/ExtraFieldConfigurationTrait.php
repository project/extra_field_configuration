<?php

namespace Drupal\extra_field_configuration;

/**
 * Trait to provide common methods associated with extra field configuration.
 *
 * If the class is capable of injecting services from the container, it should
 * inject the 'cache_tags.invalidator' service and assign it to
 * $this->cacheTagsInvalidator, inject the 'entity_type.manager' service and
 * assign it to $this->entityTypeManager, inject the
 * 'plugin.manager.extra_field_display' service and assign it to
 * $this->extraFieldManager, as well as inject the
 * 'plugin.manager.extra_field_configuration_display' service and assign it to
 * $this->extraFieldConfigurationManager.
 */
trait ExtraFieldConfigurationTrait {

  /**
   * The cache_tags.invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin.manager.extra_field_display service.
   *
   * @var \Drupal\extra_field\Plugin\ExtraFieldDisplayManager
   */
  protected $extraFieldManager;

  /**
   * The plugin.manager.extra_field_configuration_display service.
   *
   * @var \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager
   */
  protected $extraFieldConfigurationManager;

  /**
   * Gets the cache_tags.invalidator service.
   *
   * @return \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   *   The cache_tags.invalidator service.
   */
  protected function getCacheTagsInvalidator() {
    if (!$this->cacheTagsInvalidator) {
      $this->cacheTagsInvalidator = \Drupal::service('cache_tags.invalidator');
    }
    return $this->cacheTagsInvalidator;
  }

  /**
   * Gets the entity_type.manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity_type.manager service.
   */
  protected function getEntityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Gets the plugin.manager.extra_field_display service.
   *
   * @return \Drupal\extra_field\Plugin\ExtraFieldDisplayManager
   *   The plugin.manager.extra_field_display service.
   */
  protected function getExtraFieldManager() {
    if (!$this->extraFieldManager) {
      $this->extraFieldManager = \Drupal::service('plugin.manager.extra_field_display');
    }
    return $this->extraFieldManager;
  }

  /**
   * Gets the plugin.manager.extra_field_configuration_display service.
   *
   * @return \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager
   *   The plugin.manager.extra_field_configuration_display service.
   */
  protected function getExtraFieldConfigurationManager() {
    if (!$this->extraFieldConfigurationManager) {
      $this->extraFieldConfigurationManager = \Drupal::service('plugin.manager.extra_field_configuration_display');
    }
    return $this->extraFieldConfigurationManager;
  }

  /**
   * Clear all entity form caches.
   *
   * Ensures that updates to extra fields are reflected on entity edit forms.
   */
  protected function clearFormCaches() {
    // Clear the extra field cached definitions.
    $this->getExtraFieldManager()->clearCachedDefinitions();
    $this->getExtraFieldConfigurationManager()->clearCachedDefinitions();
    // We also need to invalidate the entity_field_info tag, to ensure that
    // changes to the configuration are reflected on entity display forms.
    $this->getCacheTagsInvalidator()->invalidateTags(['entity_field_info']);
  }

  /**
   * Remove extra fields from entity display forms.
   *
   * The EntityDisplayBase::removeComponent() method unfortunately sets the
   * field name to hidden, which is then exported to config. Only
   * EntityDisplayBase::onDependencyRemoval() clears the hidden fields, and it
   * will only do it if the entity type is 'field_config'. There is no public
   * method for interacting with the hidden property, so there is no way to
   * avoid the field remaining hidden after removing the extra field
   * configuration. These configuration artifacts can either be ignored or
   * removed manually.
   *
   * @param \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface $entity
   *   The configuration entity which defines the extra field configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function removeExtraFields(ExtraFieldConfigurationInterface $entity) {
    if (!empty($entity->getBundles())) {
      // Remove the field from the active displays.
      foreach ($this->getFieldActiveDisplays($entity) as $display) {
        $display->removeComponent($entity->getRealFieldName())
          ->save();
      }

      // Bundles were removed, so clear caches.
      $this->clearFormCaches();
    }
  }

  /**
   * Get the entity view displays where the extra field is configured and shown.
   *
   * @param \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface $entity
   *   The configuration entity which defines the extra field configuration.
   *
   * @return array|\Drupal\Core\Entity\Display\EntityDisplayInterface[]
   *   The entity view displays where the extra field is active.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFieldActiveDisplays(ExtraFieldConfigurationInterface $entity) {
    $active_configuration = [];

    foreach ($entity->getBundles() as $entity_id => $bundle_list) {
      foreach ($bundle_list as $bundle) {
        /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface[] $displays */
        $displays = $this->getEntityTypeManager()->getStorage('entity_view_display')
          ->loadByProperties([
            'targetEntityType' => $entity_id,
            'bundle' => $bundle,
          ]);

        // Only return displays with the extra field active.
        $displays = array_filter($displays, function ($display) use ($entity) {
          /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $display */
          return $display->getComponent($entity->getRealFieldName());
        });

        $active_configuration += $displays;
      }
    }

    return $active_configuration;
  }

}
