<?php

namespace Drupal\extra_field_configuration\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayManager;

/**
 * Manages Configurable Extra Field plugins.
 */
class ExtraFieldConfigurationDisplayManager extends ExtraFieldDisplayManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($namespaces, $cache_backend, $module_handler, $entity_type_manager);
    $this->alterInfo('extra_field_configuration_display_info');
    $this->setCacheBackend(
      $cache_backend,
      'extra_field_configuration_display_plugins',
      ['extra_field_configuration']
    );
  }

  /**
   * Gets all base (non-extra field configuration derived) plugin definitions.
   *
   * @return \Drupal\extra_field\Plugin\ExtraFieldDisplayInterface[]
   *   An array of plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   */
  public function getBaseDefinitions() {
    return array_filter($this->getParentDefinitions(), function ($definition) {
      return !isset($definition['derived']) || !$definition['derived'];
    });
  }

  /**
   * Gets all extra field configuration derived plugin definitions.
   *
   * @return \Drupal\extra_field\Plugin\ExtraFieldDisplayInterface[]
   *   An array of plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   */
  public function getDefinitions() {
    return array_filter($this->getParentDefinitions(), function ($definition) {
      return isset($definition['derived']) && $definition['derived'];
    });
  }

  /**
   * Return parent method plugin definitions.
   *
   * @return mixed[]
   *   An array of parent plugin definitions (empty array if no definitions
   *   were found). Keys are plugin IDs.
   */
  public function getParentDefinitions() {
    return parent::getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    // Find only extra field plugins with derivatives defined.
    $definitions = array_filter($definitions, function ($definition) {
      return isset($definition['deriver']) && $definition['deriver'] === 'Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationDeriver';
    });
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function entityView(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, $viewMode) {
    foreach ($build as $key => $content) {
      if (strpos($key, ':') !== FALSE && strpos($key, 'extra_field') !== FALSE) {
        $build['extra_field_' . explode(':', $key)[1]] = $content;
        unset($build[$key]);
      }
    }
  }

}
