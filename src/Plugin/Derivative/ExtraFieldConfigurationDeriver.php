<?php

namespace Drupal\extra_field_configuration\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides custom configuration plugin definitions for all extra field types.
 */
class ExtraFieldConfigurationDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct the extra_field_configuration deriver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $plugin_id = $base_plugin_definition['id'];

    // Add the original, non-derived, plugin to the list.
    $this->derivatives[$plugin_id] = $base_plugin_definition;

    /** @var \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface[] $configuration */
    $configuration = $this->entityTypeManager
      ->getStorage('extra_field_configuration')
      ->loadMultiple();

    // Add each configured bundle to the available bundles in the form
    // entity.bundle.
    foreach ($configuration as $config) {
      if ($config->getPluginId() == $plugin_id) {
        $this->derivatives[$config->id()] = $base_plugin_definition;
        $this->derivatives[$config->id()]['bundles'] = $config->getBundlesFormatted();
        $this->derivatives[$config->id()]['id'] = $config->id();
        $this->derivatives[$config->id()]['label'] = $config->label();
        $this->derivatives[$config->id()]['derived'] = TRUE;
      }
    }

    return $this->derivatives;
  }

}
