<?php

namespace Drupal\extra_field_configuration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\extra_field_configuration\ExtraFieldConfigurationInterface;

/**
 * Defines the ExtraFieldConfiguration entity.
 *
 * @ConfigEntityType(
 *   id = "extra_field_configuration",
 *   label = @Translation("Extra Field Configuration"),
 *   label_collection = @Translation("Extra Fields Configurations"),
 *   label_singular = @Translation("extra field configuration"),
 *   label_plural = @Translation("extra field configurations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count extra field configuration",
 *     plural = "@count extra field configurations",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\extra_field_configuration\Controller\ExtraFieldConfigurationListBuilder",
 *     "form" = {
 *       "add" = "Drupal\extra_field_configuration\Form\ExtraFieldConfigurationForm",
 *       "edit" = "Drupal\extra_field_configuration\Form\ExtraFieldConfigurationForm",
 *       "delete" = "Drupal\extra_field_configuration\Form\ExtraFieldConfigurationDeleteForm",
 *     }
 *   },
 *   config_prefix = "display",
 *   admin_permission = "administer extra fields",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "plugin_id" = "plugin_id",
 *     "bundles" = "bundles",
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/extra-field/edit/{extra_field_configuration}",
 *     "delete-form" = "/admin/structure/extra-field/{extra_field_configuration}/delete",
 *     "collection" = "/admin/structure/extra-field",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "plugin_id",
 *     "bundles",
 *   },
 * )
 */
class ExtraFieldConfiguration extends ConfigEntityBase implements ExtraFieldConfigurationInterface {

  /**
   * The entity id. This is also the field name used on in Twig templates.
   *
   * @var string
   */
  protected $id;

  /**
   * The entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The ID of the extra field plugin this entity configures.
   *
   * @var string
   */
  protected $plugin_id;

  /**
   * An array of entity types and bundles the configuration is attached to.
   *
   * @var array
   *
   * Array structure:
   * @code
   * $bundles = [
   *   'entity_id' => ['bundle'],
   *   'entity_id' => ['bundle'],
   * ]
   * @endcode
   */
  protected $bundles;

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId(string $plugin_id) {
    $this->plugin_id = $plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundles(string $entity_id) {
    return $this->bundles[$entity_id] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles() {
    return $this->bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundlesFormatted() {
    $formatted_bundles = [];
    foreach ($this->bundles as $entity_id => $bundles) {
      foreach ($bundles as $bundle) {
        $formatted_bundles[] = "{$entity_id}.{$bundle}";
      }
    }
    return $formatted_bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function setBundles(array $bundles) {
    $this->bundles = $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function setBundle(string $entity_type, array $bundles) {
    $this->bundles[$entity_type] = $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getRealFieldName() {
    return "extra_field_{$this->getPluginId()}:{$this->id()}";
  }

}
