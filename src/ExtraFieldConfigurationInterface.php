<?php

namespace Drupal\extra_field_configuration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines an interface for extra_field_configuration entities.
 */
interface ExtraFieldConfigurationInterface extends ConfigEntityInterface {

  /**
   * Get the extra field plugin ID this configuration manages.
   *
   * @return string
   *   The extra field plugin ID.
   */
  public function getPluginId();

  /**
   * Set the extra field plugin ID this configuration manages.
   *
   * @param string $plugin_id
   *   The plugin id to set.
   */
  public function setPluginId(string $plugin_id);

  /**
   * Get all configured bundles of an entity id that this configuration manages.
   *
   * @param string $entity_id
   *   The entity id to get bundles for.
   *
   * @return array
   *   An array of all bundles for the entity id.
   */
  public function getEntityBundles(string $entity_id);

  /**
   * Get all configured bundles this configuration manages.
   *
   * @return array
   *   A multi-dimensional array of all bundles for this configuration, keyed
   *   by entity id.
   */
  public function getBundles();

  /**
   * Get all bundles in a format expected by extra_field.
   *
   * @see \Drupal\extra_field\Annotation\ExtraFieldDisplay
   *
   * @return array
   *   An array in the form expected by ExtraFieldDisplay.
   */
  public function getBundlesFormatted();

  /**
   * Set the configured bundles this configuration manages.
   *
   * @param array $bundles
   *   A multi-dimensional array of all bundles for this configuration, keyed
   *   by entity id.
   */
  public function setBundles(array $bundles);

  /**
   * Get real (machine) field name for this extra field configuration.
   *
   * The real field name is the field name key which is stored in configuration,
   * not the nice field name which is the plugin id.
   *
   * @return string
   *   The field name, which is in the format
   *   "extra_field_{extra_field_plugin_id}:{field_name}".
   */
  public function getRealFieldName();

}
