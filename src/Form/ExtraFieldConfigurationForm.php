<?php

namespace Drupal\extra_field_configuration\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayManager;
use Drupal\extra_field_configuration\ExtraFieldConfigurationTrait;
use Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the configuration form for extra_field_configuration entities.
 */
class ExtraFieldConfigurationForm extends EntityForm {

  use ExtraFieldConfigurationTrait;

  /**
   * The cache_tags.invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The entity_type.bundle.info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleManager;

  /**
   * The entity_field.manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin.manager.extra_field_display service.
   *
   * @var \Drupal\extra_field\Plugin\ExtraFieldDisplayManager
   */
  protected $extraFieldManager;

  /**
   * The plugin.manager.extra_field_configuration_display service.
   *
   * @var \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager
   */
  protected $extraFieldConfigurationManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Construct the extra_field_configuration edit form object.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache_tags.invalidator service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_manager
   *   The entity_type.bundle.info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity_field.manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\extra_field\Plugin\ExtraFieldDisplayManager $extra_field_manager
   *   The plugin.manager.extra_field_display service.
   * @param \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager $extra_field_configuration_manager
   *   The plugin.manager.extra_field_configuration_display service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    CacheTagsInvalidatorInterface $cache_tags_invalidator,
    EntityTypeBundleInfoInterface $entity_bundle_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ExtraFieldDisplayManager $extra_field_manager,
    ExtraFieldConfigurationDisplayManager $extra_field_configuration_manager,
    MessengerInterface $messenger
  ) {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->entityBundleManager = $entity_bundle_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->extraFieldManager = $extra_field_manager;
    $this->extraFieldConfigurationManager = $extra_field_configuration_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache_tags.invalidator'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.extra_field_display'),
      $container->get('plugin.manager.extra_field_configuration_display'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit extra field instance %title', [
        '%title' => $this->entity->label(),
      ]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Extra Field Provider'),
      '#options' => [
        '' => $this->t('Select Extra Field'),
      ] + $this->getExtraFieldsAsOptions(),
      '#default_value' => $this->entity->getPluginId(),
      '#required' => TRUE,
    ];

    $form['bundles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enable On:'),
      'advanced' => [
        '#type' => 'details',
        '#title' => $this->t('Advanced'),
        '#weight' => 10,
      ],
    ];

    // Add the entity types to the render array. If it's a priority entity type,
    // we add it to the top level. If it's not, we add it in the advanced
    // section.
    foreach ($this->getFieldableEntityTypes() as $entity_type) {
      if (!in_array($entity_type->id(), $this->priorityEntities())) {
        $this->addBundles($form['bundles']['advanced'], $entity_type);
      }
      else {
        $this->addBundles($form['bundles'], $entity_type);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // Check the fieldable entity types for data.
    foreach ($this->getFieldableEntityTypes() as $entity_type) {
      $entity_id = $entity_type->id();
      $values = $form_state->getValue($entity_id);
      // Remove empty values before saving so we only save active bundles.
      $this->entity->setBundle($entity_id, array_filter($values));
    }

    try {
      $this->entity->save();
      $this->messenger->addMessage($this->t('Extra field instance %label has been saved.', [
        '%label' => $this->entity->label(),
      ]));
      $this->clearFormCaches();
    }
    catch (EntityStorageException $e) {
      $this->messenger->addMessage($this->t('Extra field instance %label was not saved.', [
        '%label' => $this->entity->label(),
      ]), MessengerInterface::TYPE_ERROR);
      throw $e;
    }
    finally {
      $form_state->setRedirect('entity.extra_field_configuration.collection');
    }
  }

  /**
   * Get an array of available extra fields as options for a select list.
   *
   * @return array
   *   An array keyed by extra field ID with the extra field label as the value.
   */
  protected function getExtraFieldsAsOptions() {
    $options = [];
    foreach ($this->extraFieldConfigurationManager->getBaseDefinitions() as $field) {
      $options[$field['id']] = $field['label'];
    }
    return $options;
  }

  /**
   * Check if the configuration entity exists in the database.
   *
   * @param string $id
   *   The entity id to check.
   *
   * @return bool
   *   True if it exists, false if it does not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(string $id) {
    return (bool) $this->entityTypeManager->getStorage('extra_field_configuration')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Get all entity types with user-configurable fields.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entity objects.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFieldableEntityTypes() {
    $entity_types = [];

    // Get all entity view display entities. These entities will be used to
    // build a list of entity types and bundles that have user-configurable
    // display fields.
    $displays = $this->entityTypeManager->getStorage('entity_view_display')
      ->loadMultiple();

    // Add each entity type once.
    foreach ($displays as $display) {
      $target_entity_id = $display->get('targetEntityType');
      if (!isset($entity_types[$target_entity_id])) {
        $entity_types[$target_entity_id] = $this->entityTypeManager->getDefinition($target_entity_id);
      }
    }

    // Sort alphabetically.
    usort($entity_types, function ($a, $b) {
      /** @var \Drupal\Core\Entity\EntityInterface $a */
      /** @var \Drupal\Core\Entity\EntityInterface $b */
      return strcasecmp($a->id(), $b->id());
    });

    return $entity_types;
  }

  /**
   * Add entities as checkboxes to the form render array.
   *
   * @param array $row
   *   The form row to add checkboxes to.
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   The entity type to add checkboxes for.
   */
  protected function addBundles(array &$row, ContentEntityTypeInterface $entity_type) {
    $entity_id = $entity_type->id();

    // Setup the row render array.
    $row[$entity_id] = [
      '#type' => 'details',
      '#title' => $entity_type->getLabel(),
      $entity_id => [
        '#type' => 'checkboxes',
        '#options' => [],
        '#default_value' => $this->entity->getEntityBundles($entity_id),
      ],
    ];

    // Add bundles to each row.
    $bundles = $this->entityBundleManager->getBundleInfo($entity_id);
    foreach ($bundles as $bundle_id => $bundle) {
      $row[$entity_id][$entity_id]['#options'][$bundle_id] = $bundle['label'];
    }
  }

  /**
   * Get a list of priority entities.
   *
   * Priority entities show at the top level of the edit form.
   *
   * @return string[]
   *   An array of entity types.
   */
  protected function priorityEntities() {
    return [
      'block_content',
      'node',
      'paragraph',
    ];
  }

}
