<?php

namespace Drupal\extra_field_configuration\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\extra_field\Plugin\ExtraFieldDisplayManager;
use Drupal\extra_field_configuration\ExtraFieldConfigurationTrait;
use Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the delete form for extra_field_configuration entities.
 */
class ExtraFieldConfigurationDeleteForm extends EntityConfirmFormBase {

  use ExtraFieldConfigurationTrait;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface
   */
  protected $entity;

  /**
   * The cache_tags.invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin.manager.extra_field_display service.
   *
   * @var \Drupal\extra_field\Plugin\ExtraFieldDisplayManager
   */
  protected $extraFieldManager;

  /**
   * The plugin.manager.extra_field_configuration_display service.
   *
   * @var \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager
   */
  protected $extraFieldConfigurationManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Construct the extra_field_configuration edit form object.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache_tags.invalidator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\extra_field\Plugin\ExtraFieldDisplayManager $extra_field_manager
   *   The plugin.manager.extra_field_display service.
   * @param \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager $extra_field_configuration_manager
   *   The plugin.manager.extra_field_configuration_display service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    CacheTagsInvalidatorInterface $cache_tags_invalidator,
    EntityTypeManagerInterface $entity_type_manager,
    ExtraFieldDisplayManager $extra_field_manager,
    ExtraFieldConfigurationDisplayManager $extra_field_configuration_manager,
    MessengerInterface $messenger
  ) {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->entityTypeManager = $entity_type_manager;
    $this->extraFieldManager = $extra_field_manager;
    $this->extraFieldConfigurationManager = $extra_field_configuration_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache_tags.invalidator'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.extra_field_display'),
      $container->get('plugin.manager.extra_field_configuration_display'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['description'][] = [
      '#type' => 'inline_template',
      '#template' => '<ul>{% for display in displays %}<li>{{ display }}</li>{% endfor %}</ul>',
      '#context' => ['displays' => $this->getActiveDisplayNames()],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the extra field instance %label?', [
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you want to delete the extra field instance %label? This action will remove all extra field configuration and cannot be undone. %label is used on the following entity displays:', [
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.extra_field_configuration.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->entity->delete();
      $this->messenger->addMessage($this->t('Extra field instance %label has been deleted.', [
        '%label' => $this->entity->label(),
      ]));
      $this->removeExtraFields($this->entity);
      $this->clearFormCaches();
    }
    catch (EntityStorageException $e) {
      $this->messenger->addMessage($this->t('Extra field instance %label was not deleted.', [
        '%label' => $this->entity->label(),
      ]), MessengerInterface::TYPE_ERROR);
      throw $e;
    }
    finally {
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
  }

  /**
   * Get all active display names the extra field configuration appears on.
   *
   * @return string[]
   *   An array of all view display config strings.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getActiveDisplayNames() {
    return array_keys($this->getFieldActiveDisplays($this->entity));
  }

}
