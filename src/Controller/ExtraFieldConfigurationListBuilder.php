<?php

namespace Drupal\extra_field_configuration\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\extra_field_configuration\ExtraFieldConfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the extra_field_configuration list builder class.
 */
class ExtraFieldConfigurationListBuilder extends ConfigEntityListBuilder {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ExtraFieldConfigurationListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($entity_type, $this->entityTypeManager->getStorage($entity_type->id()));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Administration Name'),
      'id' => $this->t('Field Name'),
      'plugin_id' => $this->t('Extra Field Provider'),
      'bundles' => $this->t('Appears On'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface $entity */
    return [
      'label' => $entity->label(),
      'id' => "extra_field_{$entity->id()}",
      'plugin_id' => $entity->getPluginId(),
      'bundles' => [
        'data' => $this->formatBundles($entity),
      ],
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if (isset($operations['edit']['title'])) {
      $operations['edit']['title'] = $this->t('Edit instance');
    }
    if (isset($operations['delete']['title'])) {
      $operations['delete']['title'] = $this->t('Delete instance');
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('No extra fields instances configured. <a href=":link">Add extra field instance</a>.', [
      ':link' => Url::fromRoute('entity.extra_field_configuration.add_form')->toString(),
    ]);
    return $build;
  }

  /**
   * Format bundles for the listing table summary.
   *
   * @param \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface $entity
   *   The extra_field_configuration entity for the row.
   *
   * @return array
   *   A render array of the row content.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function formatBundles(ExtraFieldConfigurationInterface $entity) {
    $bundles = [];

    foreach ($entity->getBundles() as $entity_type => $bundle) {
      if (!empty($bundle)) {
        $entity_storage = $this->entityTypeManager->getStorage($entity_type);
        $bundles[] = $this->t('<strong>@entity</strong>: @bundles', [
          '@entity' => $entity_storage->getEntityType()->getLabel(),
          '@bundles' => implode(', ', $bundle),
        ]);
      }
    }

    return [
      '#type' => 'inline_template',
      '#template' => '{{ bundles|safe_join("<br>") }}',
      '#context' => ['bundles' => $bundles],
    ];
  }

}
