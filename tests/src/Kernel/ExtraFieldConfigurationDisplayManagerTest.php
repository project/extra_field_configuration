<?php

namespace Drupal\Tests\extra_field_configuration\Kernel;

use Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for the Extra Field Configuration Display Manager.
 *
 * @coversDefaultClass \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager
 */
class ExtraFieldConfigurationDisplayManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'extra_field',
    'extra_field_configuration',
  ];

  /**
   * The plugin manager under test.
   *
   * @var \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager|MockObject
   */
  protected $extraFieldConfigurationDisplayManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->extraFieldConfigurationDisplayManager = $this->createPartialMock(
      ExtraFieldConfigurationDisplayManager::class,
      [
        'getParentDefinitions',
      ]
    );
  }

  /**
   * @covers \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager::getBaseDefinitions
   *
   * @dataProvider definitionsProvider
   *
   * @param array[] $definitions
   *   An array of test definitions.
   * @param array $results
   *   An array of results to check against.
   */
  public function testGetBaseDefinitions(array $definitions, array $results) {
    $this->setupParentDefinitions($definitions);
    self::assertSame(
      count($this->extraFieldConfigurationDisplayManager->getBaseDefinitions()),
      $results['base']
    );
  }

  /**
   * @covers \Drupal\extra_field_configuration\Plugin\ExtraFieldConfigurationDisplayManager::getDefinitions
   *
   * @dataProvider definitionsProvider
   *
   * @param array[] $definitions
   *   An array of test definitions.
   * @param array $results
   *   An array of results to check against.
   */
  public function testGetDefinitions(array $definitions, array $results) {
    $this->setupParentDefinitions($definitions);
    self::assertSame(
      count($this->extraFieldConfigurationDisplayManager->getDefinitions()),
      $results['derived']
    );
  }

  /**
   * Prepare ::getParentDefinitions to return mocked values.
   *
   * @param array[] $definitions
   *   An array of definitions the method should return.
   */
  protected function setupParentDefinitions(array $definitions) {
    $this->extraFieldConfigurationDisplayManager
      ->expects($this->any())
      ->method('getParentDefinitions')
      ->willReturn($definitions);
  }

  /**
   * Provide definitions and results to test against.
   *
   * @return array[]
   *   A multidimensional array of test definitions and results.
   */
  public static function definitionsProvider() {
    $definitions = [];

    // First test cases.
    $definitions[] = [
      // Definitions.
      [
        'base test a' => [
          'id' => 'base_test_a',
          'bundles' => [],
          'label' => 'base test a',
          'weight' => 0,
          'visible' => FALSE,
        ],
        'base test b' => [
          'id' => 'derived_test_b',
          'bundles' => [],
          'label' => 'derived test b',
          'weight' => 0,
          'visible' => FALSE,
          'deriver' => 'Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationNonExistentDeriver',
          'derived' => FALSE,
        ],
        'derived test a' => [
          'id' => 'derived_test_a',
          'bundles' => [],
          'label' => 'derived test a',
          'weight' => 0,
          'visible' => FALSE,
          'deriver' => 'Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationDeriver',
          'derived' => TRUE,
        ],
      ],
      // Results.
      [
        'base' => 2,
        'derived' => 1,
      ],
    ];

    // Second test cases.
    $definitions[] = [
      // Definitions.
      [
        'base test a' => [
          'id' => 'base_test_a',
          'bundles' => ['node.page'],
          'label' => 'base test a',
          'weight' => 100,
          'visible' => TRUE,
        ],
        'derived test a' => [
          'id' => 'derived_test_a',
          'bundles' => ['node.page'],
          'label' => 'derived test a',
          'weight' => 100,
          'visible' => TRUE,
          'derived' => FALSE,
        ],
        'derived test b' => [
          'id' => 'derived_test_b',
          'bundles' => ['node.page'],
          'label' => 'derived test b',
          'weight' => 100,
          'visible' => TRUE,
          'derived' => TRUE,
        ],
      ],
      // Results.
      [
        'base' => 2,
        'derived' => 1,
      ],
    ];

    return $definitions;
  }

}
