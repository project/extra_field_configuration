<?php

namespace Drupal\Tests\extra_field_configuration\Functional;

/**
 * Test the display of configurable extra fields.
 *
 * @group extra_field_configuration
 */
class ExtraFieldConfigurationFieldTest extends ExtraFieldConfigurationBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'extra_field',
    'extra_field_configuration',
    'extra_field_configuration_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupNodes();
  }

  /**
   * Test assertions for the first test node.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException;
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function testNodeOne() {
    $url = $this->testNodes['test_content_type_one']->toUrl();
    $this->drupalGet($url);

    $this->assertSession()->pageTextContains('Test Configurable Field All');
    $this->assertSession()->pageTextContains('Test Configurable Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Field None');

    $this->assertSession()->pageTextContains('Test Configurable Formatted Field All');
    $this->assertSession()->pageTextContains('Test Configurable Formatted Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field None');
  }

  /**
   * Test assertions for the second test node.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function testNodeTwo() {
    $url = $this->testNodes['test_content_type_two']->toUrl();
    $this->drupalGet($url);

    $this->assertSession()->pageTextContains('Test Configurable Field All');
    $this->assertSession()->pageTextNotContains('Test Configurable Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Field None');

    $this->assertSession()->pageTextContains('Test Configurable Formatted Field All');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field None');
  }

  /**
   * Create nodes for tests.
   */
  protected function setupNodes() {
    foreach ($this->contentTypes as $content_type) {
      $this->testNodes[$content_type] = $this->drupalCreateNode(['type' => $content_type]);
    }
  }

}
