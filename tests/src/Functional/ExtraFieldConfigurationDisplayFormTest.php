<?php

namespace Drupal\Tests\extra_field_configuration\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;

/**
 * Tests for Display Form changes added by Extra Field Configuration.
 */
class ExtraFieldConfigurationDisplayFormTest extends ExtraFieldConfigurationBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'extra_field',
    'extra_field_configuration',
    'extra_field_configuration_test',
    'field_ui',
    'node',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupDisplays();
    $user = $this->createUser(['administer node display']);
    $this->drupalLogin($user);
  }

  /**
   * Test assertions for the first content type display form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testNodeOne() {
    $this->drupalGet($this->displayUrl('test_content_type_one'));

    $this->assertSession()->pageTextContains('Test Configurable Field All');
    $this->assertSession()->pageTextContains('Test Configurable Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Field None');

    $this->assertSession()->pageTextContains('Test Configurable Formatted Field All');
    $this->assertSession()->pageTextContains('Test Configurable Formatted Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field None');
  }

  /**
   * Test assertions for the second content type display form.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testNodeTwo() {
    $this->drupalGet($this->displayUrl('test_content_type_two'));

    $this->assertSession()->pageTextContains('Test Configurable Field All');
    $this->assertSession()->pageTextNotContains('Test Configurable Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Field None');

    $this->assertSession()->pageTextContains('Test Configurable Formatted Field All');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field One');
    $this->assertSession()->pageTextNotContains('Test Configurable Formatted Field None');
  }

  /**
   * Generate the display admin screen path for a content type.
   *
   * @param string $content_type
   *   The content type to generate the path for.
   *
   * @return string
   *   The full path.
   */
  protected function displayUrl(string $content_type) {
    return "admin/structure/types/manage/{$content_type}/display";
  }

  /**
   * Create node entity view displays for tests.
   */
  protected function setupDisplays() {
    foreach ($this->contentTypes as $content_type) {
      EntityViewDisplay::create([
        'targetEntityType' => 'node',
        'bundle' => $content_type,
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }
  }

}
