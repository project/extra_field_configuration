<?php

namespace Drupal\Tests\extra_field_configuration\Functional;

use Drupal\extra_field_configuration\Entity\ExtraFieldConfiguration;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class for all Extra Field Configuration browser tests.
 */
abstract class ExtraFieldConfigurationBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An array of content types to create for tests.
   *
   * Additionally, one node of each type will be created.
   *
   * @var string[]
   */
  protected $contentTypes = [
    'test_content_type_one',
    'test_content_type_two',
  ];

  /**
   * An array of test nodes, keyed by content type.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $testNodes = [];

  /**
   * Configuration entity values, used to create test configuration entities.
   *
   * @var array[]
   */
  protected $extraFieldConfigurationEntities = [
    'test_configurable_field_all' => [
      'label' => 'Test Configurable Field All',
      'bundles' => [
        'node' => ['test_content_type_one', 'test_content_type_two'],
      ],
    ],
    'test_configurable_field_one' => [
      'label' => 'Test Configurable Field One',
      'bundles' => [
        'node' => ['test_content_type_one'],
      ],
    ],
    'test_configurable_field_none' => [
      'label' => 'Test Configurable Field None',
      'bundles' => [],
    ],
    'test_configurable_formatted_field_all' => [
      'label' => 'Test Configurable Formatted Field All',
      'bundles' => [
        'node' => ['test_content_type_one', 'test_content_type_two'],
      ],
    ],
    'test_configurable_formatted_field_one' => [
      'label' => 'Test Configurable Formatted Field One',
      'bundles' => [
        'node' => ['test_content_type_one'],
      ],
    ],
    'test_configurable_formatted_field_none' => [
      'label' => 'Test Configurable Formatted Field None',
      'bundles' => [],
    ],
  ];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupExtraFields();
    $this->setupContentTypes();
  }

  /**
   * Create content types for tests.
   */
  protected function setupContentTypes() {
    foreach ($this->contentTypes as $content_type) {
      $this->drupalCreateContentType(['type' => $content_type]);
    }
  }

  /**
   * Create extra field configurations for tests.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setupExtraFields() {
    foreach ($this->extraFieldConfigurationEntities as $field_id => $values) {
      /** @var \Drupal\extra_field_configuration\ExtraFieldConfigurationInterface $extra_field */
      $extra_field = ExtraFieldConfiguration::create([
        'id' => $field_id,
        'plugin_id' => $field_id,
        'label' => $values['label'],
        'bundles' => $values['bundles'],
      ]);
      $extra_field->save();
    }
  }

}
