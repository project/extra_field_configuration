<?php

namespace Drupal\extra_field_configuration_test\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;

/**
 * Test extra field.
 *
 * @ExtraFieldDisplay(
 *   id = "test_configurable_field_one",
 *   label = @Translation("Test Configurable Field One"),
 *   deriver = "Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationDeriver",
 *   visible = true,
 * )
 */
class TestFieldOne extends ExtraFieldDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {
    return [
      '#markup' => 'Test Configurable Field One',
    ];
  }

}
