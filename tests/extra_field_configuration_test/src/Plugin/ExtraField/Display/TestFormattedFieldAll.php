<?php

namespace Drupal\extra_field_configuration_test\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayFormattedBase;

/**
 * Test extra formatted field.
 *
 * @ExtraFieldDisplay(
 *   id = "test_configurable_formatted_field_all",
 *   label = @Translation("Test Configurable Formatted Field All"),
 *   deriver = "Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationDeriver",
 *   visible = true,
 * )
 */
class TestFormattedFieldAll extends ExtraFieldDisplayFormattedBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(ContentEntityInterface $entity) {
    return [
      '#markup' => 'Test Configurable Formatted Field All',
    ];
  }

}
