# Extra Field Configuration

This module provides a configuration layer for [Extra Field](https://www.drupal.org/project/extra_field)
to allow developers to manage the placement of extra fields via configuration
rather than in annotation properties. The module also allows for the usage of
multiple instances of the same extra field plugin on an entity, without having
to define multiple instances of the plugin in code.

## Instructions

### Requirements
[Extra Field](https://www.drupal.org/project/extra_field)

### Installation
1. Add the module to your project with `composer require drupal
  /extra_field_configuration`, or manually download the source and place it in
  `modules/contrib`.
1. Enable the module.

### Configuration
Create extra field instances from extra field plugins and manage entity
assignments at `/admin/structure/extra-field`.

### Plugin Changes
To enable an extra field plugin to be configured by this module, the `deriver`
property must be present on the annotation. It is also recommended to omit
`bundles` property, to ensure the extra field is not managed in two different
ways.

A complete Extra Field Configuration plugin annotation would look like:

```
* @ExtraFieldDisplay(
*   id = "plugin_id",
*   label = @Translation("Field name"),
*   deriver = "Drupal\extra_field_configuration\Plugin\Derivative\ExtraFieldConfigurationDeriver",
*   weight = 10,
*   visible = false,
* )
```

The class definitions of the extra field plugins remains unchanged with this
module, and [Extra Field Plus](https://www.drupal.org/project/extra_field_plus)
plugins can also be used. The location of plugins class files also remains the
same.

### Printing in Templates
Field names for printing in templates can be found on the admin screen at
`/admin/structure/extra-field`. The field name is generated from the machine
name defined when creating the extra field instance.

## Examples
The included **extra_field_configuration_examples** module provides example
extra field plugins in both simple and formatted flavors.

## Maintainers
* [Benjamin Baird](https://www.drupal.org/u/benabaird)
